<?php

namespace app\models;

use Yii ;
use yii\behaviors\TimestampBehavior ;
use yii\db\ActiveRecord ;
use app\models\Number ;

/**
* Забронированные номера.
*/
class NumberBooked extends ActiveRecord {
	/**
	* @param string $tableName - название таблицы в БД
	*/
	protected $tableName = 'number_booked' ;
	protected $primaryKey = [ 'id' ] ;

	/**
	* @return array the validation rules.
	*/
	public function rules( ) {
		return [
			[ [ 'number_id' , 'book_date' , 'fio' , 'phone' , ] , 'required' , ] ,
			[ [ 'id' , 'number_id' , 'phone' , ] , 'integer' , ] ,
			[ [ 'book_date' , ] , 'date' , 'format' => 'php:Y-m-d' , ] ,
			[ [ 'number_id' , ] , 'exist' , 'skipOnError' => true ,
				'targetClass' => Number::className( ) ,
				'targetAttribute' => [ 'number_id' => 'id' , ] ,
			] ,
		] ;
	}
	/**
	* {@inheritdoc}
	*/
	public function behaviors( ) {
		return [
			TimestampBehavior::className( ) ,
		];
	}

	/**
	* @return array customized attribute labels
	*/
	public function attributeLabels( ) {
		return [
			'id' => '#' ,
			'number_id' => 'номер' ,
			'book_date' => 'дата, на когда забронировано' ,
			'fio' => 'ФИО клиента' ,
			'phone' => 'телефон клиента' ,
		] ;
	}
}
