<?php

namespace app\models ;

use Yii ;
use yii\db\ActiveRecord ;

/**
* Номер.
*/
class Number extends ActiveRecord {
	/**
	* @param string $tableName - название таблицы в БД
	*/
	protected $tableName = 'number' ;

	/**
	* @return array the validation rules.
	*/
	public function rules( ) {
		return [
			[ [ 'title' , 'comment' , ] , 'required' ] ,
			[ [ 'id' , ] , 'integer' ] ,
		] ;
	}

	/**
	* @return array customized attribute labels
	*/
	public function attributeLabels( ) {
		return [
			'title' => 'название' ,
			'comment' => 'краткое описание' ,
		] ;
	}
}
