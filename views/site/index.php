<?php
/* @var $this yii\web\View */

$this->title = 'Забронированные номера' ;
?>
<div class="site-index">
	<div class="body-content">
		<table class="table">
			<caption><?=htmlspecialchars( $this->title )?></caption>
			<thead>
				<tr>
					<th>ФИО</th>
					<th>Телефон</th>
					<th>Даты</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ( $number_booked_list as $number_booked ) { ?>
				<tr>
					<td><?=htmlspecialchars( $number_booked->fio )?></td>
					<td><?=htmlspecialchars( $number_booked->phone )?></td>
					<td><?=htmlspecialchars( $number_booked->book_date )?></td>
				</tr>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="3">
						<?=\yii\widgets\LinkPager::widget( [ 'pagination' => $pages , ] )?>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
