<?php
	use yii\helpers\Html ;
	use yii\widgets\ActiveForm ;

	$this->title = 'Редактирование номера' ;

	$this->params['breadcrumbs'] = [
		[
			'label' => 'Админка' ,
			'url' => [ '/admin' , ] ,
		] , [
			'label' => 'Список' ,
			'url' => [ 'number-list' , ] ,
		] ,
		$this->title
	] ;
?>
<h1><?=htmlspecialchars( $this->title . ' #' . $number->id )?></h1>
<?php
	if ( $errors ) {
?>
	<ul>
	<?php foreach( $errors as $error ) { ?>
		<li><?=print_r( $error , true )?></li>
	<?php } ?>
	</ul>
<?php
	}
?>
<?php $form = ActiveForm::begin( ) ; ?>
	<?=$form->field( $number , 'title' )?>
	<?=$form->field( $number , 'comment' )?>

	<?=Html::submitButton( \Yii::t( 'app', 'сохранить' ) , [
		'class' => 'btn btn-lg btn-primary btn-block' ,
	] ) ?>
<?php $form->end( ) ; ?>