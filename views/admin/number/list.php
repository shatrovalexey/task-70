<?php
/* @var $this yii\web\View */

$this->title = 'Номера' ;

$this->params['breadcrumbs'] = [
	[
		'label' => 'Админка' ,
		'url' => [ '/admin' , ] ,
	] ,
	$this->title ,
] ;

?>
<style>
.number_booked_list tbody input {
	width: 100% ;
}
</style>
<div class="site-index">
	<?=\yii\widgets\LinkPager::widget( [ 'pagination' => $pages , ] )?>
	<p><a href="number-create">создать номер</a>
	<table class="table" id="number_booked_list">
		<caption><?=htmlspecialchars( $this->title )?></caption>
		<thead>
			<tr>
				<th width="100">#</th>
				<th>название</th>
				<th>краткое описание</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ( $number_list as $number ) { ?>
			<tr>
				<td><?=$number->id?></td>
				<td><?=htmlspecialchars( $number->title )?></td>
				<td><?=htmlspecialchars( $number->comment )?></td>
				<td>
					<p><a href="number-delete?id=<?=$number->id?>">удалить</a>
					<p><a href="number-edit?id=<?=$number->id?>">редактировать</a>
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<?=\yii\widgets\LinkPager::widget( [ 'pagination' => $pages , ] )?>
</div>
