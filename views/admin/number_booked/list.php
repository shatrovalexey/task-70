<?php
/* @var $this yii\web\View */

$this->title = 'Забронированные номера' ;

$this->params['breadcrumbs'] = [
	[
		'label' => 'Админка' ,
		'url' => [ '/admin' , ] ,
	] ,
	$this->title ,
] ;

?>
<style>
.number_booked_list tbody input {
	width: 100% ;
}
</style>
<div class="site-index">
	<?=\yii\widgets\LinkPager::widget( [ 'pagination' => $pages , ] )?>
	<p><a href="number-booked-create">забронировать номер</a>
	<table class="table" id="number_booked_list">
		<caption><?=htmlspecialchars( $this->title )?></caption>
		<thead>
			<tr>
				<th>#</th>
				<th width="30%">фИО</th>
				<th width="20%">телефон</th>
				<th width="20%">дата</th>
				<th width="20%">номер</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ( $number_booked_list as $number_booked ) { ?>
			<tr>
				<td>
					<?=htmlspecialchars( $number_booked[ 'id' ] )?>
				</td>
				<td>
					<?=htmlspecialchars( $number_booked[ 'fio' ] )?>
				</td>
				<td>
					+<?=htmlspecialchars( $number_booked[ 'phone' ] )?>
				</td>
				<td>
					<?=htmlspecialchars( $number_booked[ 'book_date' ] )?>
				</td>
				<td>
					<?=htmlspecialchars( $number_booked[ 'number_title' ] )?>
				</td>
				<td>
					<p><a href="number-booked-delete?id=<?=$number_booked[ 'id' ]?>">удалить</a>
					<p><a href="number-booked-edit?id=<?=$number_booked[ 'id' ]?>">редактировать</a>
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<?=\yii\widgets\LinkPager::widget( [ 'pagination' => $pages , ] )?>
</div>
