<?php
	use yii\helpers\Html ;
	use yii\widgets\ActiveForm ;

	$this->title = 'Создание брони номера' ;

	$this->params['breadcrumbs'] = [
		[
			'label' => 'Админка' ,
			'url' => [ '/admin' , ] ,
		] , [
			'label' => 'Список' ,
			'url' => [ 'number-booked-list' , ] ,
		] ,
		$this->title ,
	] ;
?>
<h1><?=htmlspecialchars( $this->title )?></h1>
<?php if ( $errors ) { ?>
	<ul>
	<?php foreach( $errors as $error ) { ?>
		<li><?=print_r( $error , true )?></li>
	<?php } ?>
	</ul>
<?php } ?>
<?php $form = ActiveForm::begin( ) ; ?>
	<?=$form->field( $number_booked , 'fio' )?>
	<?=$form->field( $number_booked , 'phone' )?>
	<?=$form->field( $number_booked , 'book_date' )->textInput( [
		'type' => 'date' ,
	] )?>
	<?=$form->field( $number_booked , 'number_id' )
		->dropDownList( \yii\helpers\ArrayHelper::map( $number_list , 'id' , 'title' ) )?>

	<?=Html::submitButton( \Yii::t( 'app', 'сохранить' ) , [
		'class' => 'btn btn-lg btn-primary btn-block' ,
	] ) ?>
<?php $form->end( ) ; ?>