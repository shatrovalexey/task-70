<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Number ;
use app\models\NumberBooked ;
use app\models\User ;
use yii\db\Query ;

class SiteController extends Controller {
    /**
     * {@inheritdoc}
     */
    public function behaviors( ) {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions( ) {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex( ) {
	$query = NumberBooked::find( )
		->select( [ 'fio' , 'phone' , 'group_concat( DISTINCT book_date ORDER BY 1 DESC ) AS book_date' ] )
		->orderBy( [ 'fio' => 'ASC' , 'phone' => 'ASC' ] )
		->groupBy( [ 'fio' , 'phone' ] ) ;

	$totalCount = $query->count( ) ;

	$pages = new \yii\data\Pagination( [
		'totalCount' => $totalCount ,
		'pageSize' => 10 ,
	] ) ;

	$number_booked_list = $query
		->offset( $pages->offset )->limit( $pages->limit )->all( ) ;

	return $this->render( 'index' , [
		'pages' => $pages ,
		'number_booked_list' => $number_booked_list ,
	] ) ;
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ( $model->load( Yii::$app->request->post( ) ) && $model->login( ) ) {
		if ( \Yii::$app->user->identity->role_id == User::ROLE_ADMIN ) {
			return $this->redirect( '/admin' ) ;
		}

		return $this->goBack( ) ;
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
