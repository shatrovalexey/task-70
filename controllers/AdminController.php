<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Number ;
use app\models\NumberBooked ;
use app\models\User ;
use yii\db\Query ;

class AdminController extends Controller {
    /**
     * {@inheritdoc}
     */
    public function behaviors( ) {
        return [
            'access' => [
                'class' => AccessControl::className( ) ,
                'only' => [ 'logout' , 'signup' , ] ,
                'rules' => [
                    [
                        'actions' => [ 'signup' , 'login' , ] ,
                        'allow' => true ,
                        'roles' => [ '?' ] ,
                    ] , [
                        'actions' => [ 'logout' , ] ,
                        'allow' => true ,
                        'roles' => [ '@' ] ,
                    ] ,
                ] ,
            ] ,
            'verbs' => [
                'class' => VerbFilter::className( ) ,
                'actions' => [
			'logout' => [ 'post' ] ,
                ] ,
            ] ,
        ] ;
    }

	public function beforeAction( $action ) {
		if (
			! in_array( $action->actionMethod , [ 'actionLogin' ] ) &&
			( \Yii::$app->user->identity->role_id != User::ROLE_ADMIN ) ||
			! parent::beforeAction( $action )
		) {
			return $this->goHome( ) ;
		}

		return true ;
	}

    /**
     * {@inheritdoc}
     */
    public function actions( ) {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

	/**
	* Главная страница
	*/
	public function actionIndex( ) {
		return $this->render( 'index' ) ;
	}

	/**
	* Список номеров
	*/
	public function actionNumberList( ) {
		$query = Number::find( ) ;

		$totalCount = $query->count( ) ;

		$pages = new \yii\data\Pagination( [
			'totalCount' => $totalCount ,
			'pageSize' => 10 ,
		] ) ;

		$number_list = $query
				->orderBy( [ 'id' => SORT_DESC , ] )
				->offset( $pages->offset )
				->limit( $pages->limit )->all( ) ;

		return $this->render( 'number/list' , [
			'pages' => $pages ,
			'number_list' => $number_list ,
		] ) ;
	}

	/**
	* Удалить номер
	*
	* @param integer $id - идентификатор номера
	*/
	public function actionNumberDelete( $id ) {
		Number::findOne( $id )->delete( ) ;

		return $this->redirect( \Yii::$app->request->referrer ) ;
	}

	/**
	* Создать номер
	*/
	public function actionNumberCreate( ) {
		$number = new Number( ) ;
		$data = \Yii::$app->request->post( ) ;

		if ( $number->load( $data ) && $number->validate( ) ) {
			$number->save( ) ;

			return $this->render( 'number/edit' , [
				'number' => $number ,
				'errors' => [ ] ,
			] ) ;
		}

		return $this->render( 'number/create' , [
			'errors' => $number->getErrors( ) ,
			'number' => $number ,
		] ) ;
	}

	/**
	* Редактировать номер
	*
	* @param integer $id - идентификатор номера
	*/
	public function actionNumberEdit( $id ) {
		$model = new Number( ) ;
		$data = \Yii::$app->request->post( ) ;

		if ( $model->load( $data ) && $model->validate( ) ) {
			$number = Number::findOne( $model->id ) ;
			$number->title = $model->title ;
			$number->comment = $model->comment ;
			$number->update( ) ;
		}

		if ( empty( $number ) ) {
			$number = Number::findOne( $id ) ;
		}

		return $this->render( 'number/edit' , [
			'errors' => $model->getErrors( ) ,
			'number' => $number ,
		] ) ;
	}

	/**
	* Список забронированных номеров
	*/
	public function actionNumberBookedList( ) {
		$query = NumberBooked::find( )
			->innerJoin( 'number' , 'number_booked.number_id=number.id' ) ;

		$totalCount = $query->count( ) ;

		$pages = new \yii\data\Pagination( [
			'totalCount' => $totalCount ,
			'pageSize' => 10 ,
		] ) ;

		$query->select( [ 'number_booked.*' , 'number.title AS number_title' , ] )
			->orderBy( [ 'number_booked.id' => SORT_DESC , ] )
			->offset( $pages->offset )->limit( $pages->limit ) ;

		$number_booked_list = $query->asArray( )->all( ) ;

		return $this->render( 'number_booked/list' , [
			'pages' => $pages ,
			'number_booked_list' => $number_booked_list ,
		] ) ;
	}

	/**
	* Создать забронированный номер
	*/	
	public function actionNumberBookedCreate( ) {
		$number_booked = new NumberBooked( ) ;
		$number_list = Number::find( )->all( ) ;
		$data = \Yii::$app->request->post( ) ;

		if ( $number_booked->load( $data ) && $number_booked->validate( ) ) {
			$number_booked->save( ) ;

			return $this->render( 'number_booked/edit' , [
				'number_booked' => $number_booked ,
				'number_list' => $number_list ,
				'errors' => [ ] ,
			] ) ;
		}

		return $this->render( 'number_booked/create' , [
			'errors' => $number_booked->getErrors( ) ,
			'number_booked' => $number_booked ,
			'number_list' => $number_list ,
		] ) ;
	}

	/**
	* Редактировать забронированный номер
	*
	* @param integer $id - идентификатор
	*/
	public function actionNumberBookedEdit( $id ) {
		$model = new NumberBooked( ) ;
		$data = \Yii::$app->request->post( ) ;

		if ( $model->load( $data ) && $model->validate( ) ) {
			$number_booked = NumberBooked::findOne( $model->id ) ;
			$number_booked->fio = $model->fio ;
			$number_booked->phone = $model->phone ;
			$number_booked->book_date = $model->book_date ;
			$number_booked->number_id = $model->number_id ;
			$number_booked->update( ) ;
		}

		if ( empty( $number_booked ) ) {
			$number_booked = NumberBooked::findOne( $id ) ;
		}

		return $this->render( 'number_booked/edit' , [
			'errors' => $model->getErrors( ) ,
			'number_booked' => $number_booked ,
			'number_list' => Number::find( )->all( ) ,
		] ) ;
	}

	/**
	* Удалить забронированный номер
	*
	* @param integer $id - идентификатор
	*/
	public function actionNumberBookedDelete( $id ) {
		NumberBooked::findOne( $id )->delete( ) ;

		return $this->redirect( \Yii::$app->request->referrer ) ;
	}

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
