<?php

use yii\db\Migration;

/**
 * Class m200410_111138_number_taken
 */
class m200410_111138_number_taken extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp( ) {
	$dbh = \Yii::$app->db->masterPdo ;

	$dbh->exec( "
CREATE TABLE IF NOT EXISTS `number`(
	`id` BIGINT( 22 ) UNSIGNED NOT null AUTO_INCREMENT COMMENT 'идентификатор' ,
	`title` VARCHAR( 100 ) NOT null COMMENT 'название' ,
	`comment` TEXT NOT null COMMENT 'краткое описание' ,

	PRIMARY KEY( `id` )
) COMMENT = 'номер'
ENGINE = InnoDB
CHARSET = utf8mb4 ;
	" ) ;

	$dbh->exec( "
CREATE TABLE IF NOT EXISTS `number_booked`(
	`id` BIGINT( 22 ) UNSIGNED NOT null AUTO_INCREMENT COMMENT 'идентификатор' ,
	`number_id` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'идентификатор номера' ,
	`book_date` DATE NOT null COMMENT 'день, на который бронируется' ,

	`fio` VARCHAR( 100 ) NOT null COMMENT 'ФИО клиента' ,
	`phone` CHAR( 11 ) NOT null COMMENT 'телефон клиента' ,

	`created_at` INTEGER NOT null COMMENT 'дата-время создания' ,
	`updated_at` INTEGER NOT null COMMENT 'дата-время изменения' ,

	PRIMARY KEY( `id` ) ,
	UNIQUE( `number_id` , `book_date` ) ,
	INDEX( `fio` , `phone` , `number_id` ) ,
	CONSTRAINT `fk_number_booked_number`
		FOREIGN KEY ( `number_id` )
		REFERENCES `number`( `id` )
		ON DELETE CASCADE
		ON UPDATE CASCADE
) COMMENT = 'забронированный номер'
ENGINE = InnoDB
CHARSET = utf8mb4 ;
	" ) ;

	$dbh->exec( "
INSERT IGNORE INTO `number`( `title` , `comment` )
SELECT
	md5( uuid( ) ) AS `title` ,
	md5( uuid( ) ) AS `comment`
FROM
	`mysql`.`user` AS `u1` ,
	`mysql`.`user` AS `u2`
LIMIT 100 ;
	" ) ;

	$dbh->exec( "
SET @`v_day` := 60 * 60 * 24 ;
	" ) ;

	$dbh->exec( "
INSERT IGNORE INTO `number_booked`(
	`number_id` , `book_date` , `fio` , `phone` , `created_at` , `updated_at`
)
SELECT
	`n1`.`id` AS `number_id` ,
	from_unixtime( unix_timestamp( ) + ( rand( ) - 0.5 ) * rand( ) * @`v_day` * 365 ) AS `book_date` ,
	md5( uuid( ) ) AS `fio` ,
	replace( concat(
		rand( ) * 10 , rand( ) * 10 , rand( ) * 10 , rand( ) * 10 , rand( ) * 10 ,
		rand( ) * 10 , rand( ) * 10 , rand( ) * 10 , rand( ) * 10 , rand( ) * 10 , rand( ) * 10
	) , '.' , '' ) AS `phone` ,
	unix_timestamp( ) AS `created_at` ,
	unix_timestamp( ) AS `updated_at`
FROM
	`number` AS `n1` ,
	`number` AS `n2`
LIMIT 1000 ;
	" ) ;

	$dbh->exec( "
UPDATE
	`number_booked` AS `nb1`
SET
	`nb1`.`created_at` := unix_timestamp( `nb1`.`book_date` ) - rand( ) * @`v_day` ,
	`nb1`.`updated_at` := `nb1`.`created_at` ;
	" ) ;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown( ) {
	$dbh = \Yii::$app->db->masterPdo ;

	$dbh->exec( "
DROP TABLE IF EXISTS `number_booked` ;
	" ) ;

	$dbh->exec( "
DROP TABLE IF EXISTS `number` ;
	" ) ;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200410_111138_number_taken cannot be reverted.\n";

        return false;
    }
    */
}
